# Hibi 日々

[Live website](https://hibi.fun)

A website, twitter bot ([@hibianime](https://twitter.com/hibianime)) and mastodon bot ([@hibianime@botsin.space](https://botsin.space/@hibianime)) that post a pseudo-random anime character every day!

## Project direction

No immediate work is being done on the project.

Right now: (a) I am content with the functionality of both the website and bots as they currently stand and (b) am not sure exactly what more functionality I would even want to add.

# I am open to ideas and of course welcome forks, though!

# Hibi 日々 social media bots

Bots post once per day at 12:00PM CST from twitter ([@hibianime](https://twitter.com/hibianime)) and mastodon ([@hibianime@botsin.space](https://botsin.space/@hibianime))

All bot logic is in the [bots/](https://gitlab.com/spongechameleon/hibi/-/tree/twitter-bot/bots) folder

tweet -> Twitter

toot -> Mastodon
